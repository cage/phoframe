/* phoframe: make fortune strips
 Copyright (C) 2016  cage

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <Arduino.h>

#define SENSOR_PIN 12

#define BLACK       1

#define WHITE       0

#define WIDE_BAR_H  200

#define THIN_BAR_H  100

#define ACCUM       100


void init_sensor(){
  pinMode(SENSOR_PIN, INPUT);
}

int read_surface(){
  int b = 0;
  int w = 0;
  for (int i = 0; i< ACCUM; i++){
    if (digitalRead(SENSOR_PIN) == BLACK){
      b++;
    }else{
      w++;
    }
  }
  return b >= w ? BLACK : WHITE;
}


int read_surface(int accum){
  int b = 0;
  int w = 0;
  for (int i = 0; i< accum; i++){
    if (digitalRead(SENSOR_PIN) == BLACK){
      b++;
    }else{
      w++;
    }
  }
  return b >= w ? BLACK : WHITE;
}


int blackp (){
  return read_surface() == BLACK;
}

int whitep (){
  return !blackp();
}


int blackp (int accum){
  return read_surface(accum) == BLACK;
}

int whitep (int accum){
  return !blackp(accum);
}
