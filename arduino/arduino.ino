/* phoframe: make fortune strips
 Copyright (C) 2016  cage

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <Stepper.h>

#include "motor.h"

#include "reflectance_sensor.h"

#define H_BAR_THRS                       0.9

#define DELAY                      1200000 // 20 minutes

#define DELAY_START                  10000 // 10 seconds

#define TRESHOLD_WORKAROUND           1000

#define OFFSET_WORKAROUND              300

unsigned char elements_number        = 0;

unsigned char current_element_number = 0;

int           element_offset         = 0;


void reset_count () {
  elements_number        = 0;
  current_element_number = 0;
}

int count_needed_p () {
  return elements_number == 0;
}

int go_down_until_black() {
  int res = 0;

  while (whitep()){
    go_down();
    res++;
  }
  return res;
}

int go_down_until_white() {
  int res = 0;

  while (blackp()){
    go_down();
    res++;
  }
  return res;
}


int go_down_until_black(int accum) {
  int res = 0;

  while (whitep(accum)){
    go_down();
    res++;
  }
  return res;
}

int go_down_until_white(int accum) {
  int res = 0;

  while (blackp(accum)){
    go_down();
    res++;
  }
  return res;
}


int go_up_until_black() {
  int res = 0;

  while (whitep()){
    go_up();
    res++;
  }
  return res;
}

int go_up_until_white() {
  int res = 0;

  while (blackp()){
    go_up();
    res++;
  }
  return res;
}

void count_elements (){
  go_up_until_white();
  char stop = 0;
  while (!stop){
    element_offset = go_up_until_black();
    int marker_h = go_up_until_white();
    Serial.print(F("h: "));
    Serial.println(marker_h);
    if ((marker_h > (THIN_BAR_H + H_BAR_THRS * THIN_BAR_H))){
      stop = 1;
      go_down_until_black();
      go_down_until_white();
      Serial.println(F("count STOP"));
      Serial.print(F("element numbers "));
      Serial.println(elements_number);
      Serial.print(F("offset "));
      Serial.println(element_offset);
    }else if(marker_h > THIN_BAR_H / 3){
      elements_number++;
      Serial.println(elements_number);
    }
  }
}

void go_to_start (){
  char stop = 0;
  while (!stop){
    go_down_until_black();
    int marker_h = go_down_until_white();
    if ((marker_h > (THIN_BAR_H + H_BAR_THRS * THIN_BAR_H))){
      stop = 1;
      go_up_until_black();
      go_up_until_white();
      go_down_until_black();
      Serial.println(F("go start: STOP"));

    }else if(marker_h > THIN_BAR_H / 3){
      Serial.print(F("marker "));
      Serial.println(current_element_number);
    }
  }
}


void go_to_element_offset (int n){
  if(n <= 0){
    for (int i = n; i < 0; i++){
      Serial.print(F("down "));
      Serial.println(current_element_number + 1);
      unsigned long time = millis();
      go_down_until_black();
      go_down_until_white();
      go_down_until_black();
      go_up_until_white();
      unsigned long diff = millis() - time;
      Serial.println(diff);
      if(diff > TRESHOLD_WORKAROUND){
	current_element_number--;
      }else{
	Serial.print(F("workaround... "));
	go_up(OFFSET_WORKAROUND);
	i--;
      }
    }
  }else{
    for (int i = 0; i < n; i++){
      unsigned long time = millis();
      go_up_until_black();
      go_up_until_white();
      unsigned long diff = millis() - time;
      if(diff > TRESHOLD_WORKAROUND){
	current_element_number++;
      }else{
	Serial.print(F("workaround... "));
	go_down(OFFSET_WORKAROUND);
	i--;
      }
      Serial.println(current_element_number + 1);
    }
  }
}

void setup() {
  randomSeed(analogRead(0));
  Serial.begin(9600);
  pinMode(1, INPUT);
  init_motors();
  init_sensor();
}

void start (){
  reset_count();
  go_to_start();
  count_elements();
  go_to_start();
  go_up_until_white();
  Serial.println(F("+1"));
  go_to_element_offset(1);
  current_element_number=0;
  Serial.println(current_element_number + 1);
  delay(DELAY_START);
}



void loop() {
  if(count_needed_p()){
    start();
  }

  // here roll a dice
  int dice_roll = random(elements_number) - current_element_number;
  Serial.print(F("dice "));
  Serial.print(dice_roll);
  Serial.print(" ");
  Serial.println(current_element_number + 1);
  go_to_element_offset(dice_roll);
  delay(DELAY);

}
