/* phoframe: make fortune strips
 Copyright (C) 2016  cage

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#define STEPS_PER_REVOLUTION_MOTOR 32

#define STEPS_PER_REVOLUTION     2048

#define UPPER_MOTOR_PIN1            2
#define UPPER_MOTOR_PIN2            3
#define UPPER_MOTOR_PIN3            4
#define UPPER_MOTOR_PIN4            5

#define LOWER_MOTOR_PIN1            8
#define LOWER_MOTOR_PIN2            9
#define LOWER_MOTOR_PIN3           10
#define LOWER_MOTOR_PIN4           11

#define MOTOR_SPEED               400

int     steps_so_far = 0;

Stepper upper_motor(STEPS_PER_REVOLUTION_MOTOR,
		    UPPER_MOTOR_PIN1,
		    UPPER_MOTOR_PIN3,
		    UPPER_MOTOR_PIN2,
		    UPPER_MOTOR_PIN4);

Stepper lower_motor(STEPS_PER_REVOLUTION_MOTOR,
		    LOWER_MOTOR_PIN1,
		    LOWER_MOTOR_PIN3,
		    LOWER_MOTOR_PIN2,
		    LOWER_MOTOR_PIN4);

void init_motors () {
  upper_motor.setSpeed(MOTOR_SPEED);
  lower_motor.setSpeed(MOTOR_SPEED);
}


int go_down (){
  int saved_step = steps_so_far;
  lower_motor.step(1);
  upper_motor.step(1);
  steps_so_far++;
  return saved_step;
}

int go_down (int steps){
  int saved_step = steps_so_far;
  for (int i = 0; i < steps; i++){
    go_down();
  }
  return saved_step;
}

int go_up (){
  int saved_step = steps_so_far;
  upper_motor.step(-1);
  lower_motor.step(-1);
  steps_so_far--;
  return saved_step;
}


int go_up (int steps){
  int saved_step = steps_so_far;
  for (int i = 0; i < steps; i++){
    go_up();
  }
  return saved_step;
}
