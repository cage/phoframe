;; mkphoframe: make fortune strips
;; Copyright (C) 2016  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :phoframe)

(defun cmdline ()
  #+sbcl (rest sb-ext:*posix-argv*)
  #+ccl *command-line-argument-list*
  #-(or ccl sbcl) (error "implementation not supported"))

(defun exit (&optional (code 0))
  #+sbcl (sb-ext:exit :code code)
  #+ccl  (quit              code))

(defmacro process-command-line (&body body)
  `(unix-options:with-cli-options ((cmdline) t)
       ((draw-box "draw box around each item")
	unix-options:&parameters
	(font-dir "The directory where the fonts are (default: \"/usr/share/fonts/type1/gsfonts/\")")
	(font-name         "The name of the font (default: \"z003034l\")")
	(maximum-font-size "The maximum size of the font")
	(starting-message  "The starting message")
	(ending-message    "The ending message")
	unix-options:&free
	input-file)
     (setf (drawing-options-draw-box-p *drawing-options*) draw-box)
     (when font-name
       (setf (drawing-options-font-name *drawing-options*) font-name))
     (when font-dir
       (setf (drawing-options-font-dir *drawing-options*)  font-dir))
     (when (and maximum-font-size	       (parse-number:parse-number maximum-font-size))
       (setf (drawing-options-maximum-font-size *drawing-options*)
	     (parse-number:parse-number maximum-font-size)))
     (when starting-message
       (setf (drawing-options-starting-message *drawing-options*)
	     (unescape-string starting-message)))
     (when ending-message
       (setf (drawing-options-ending-message *drawing-options*)
	     (unescape-string ending-message)))
     (if (and input-file
	      (uiop:file-exists-p (elt input-file 0)))
	 (setf (drawing-options-file-path *drawing-options*) (elt input-file 0))
	 (progn
	   (format *error-output* "Error: missing input file~%")
	   (exit 0)))
     ,@body))

(cffi:defctype size :unsigned-int)

(cffi:defcallback write-to-stdout size ((doc :pointer) (data :pointer) (size size))
  (declare (ignore doc))
  (write-sequence (cffi:foreign-string-to-lisp data :count size) *standard-output*)
  size)

(defun setup-ps-doc ()
  (ps:open-doc      *ps-doc* nil)
  (ps:set-parameter *ps-doc* ps:+parameter-key-searchpath+ (font-dir))
  (ps:set-parameter *ps-doc* ps:+parameter-key-imagereuse+ ps:+false+)
  (ps:begin-page    *ps-doc*)
  (draw-fortune     *ps-doc* 0 (drawing-options-ending-message *drawing-options*))
  (incf *count-boxes*))

(defun cleanup-ps-doc ()
   (draw-fortune     *ps-doc*
		     *count-boxes*
		     (drawing-options-starting-message *drawing-options*))
   (ps:end-page  *ps-doc*)
   (ps:close-doc *ps-doc*)
   (ps:shutdown))

(defun main ()
  (let* ((*drawing-options* (make-drawing-options)))
    (process-command-line
     (let* ((*count-boxes*   0)
	    (*lines-in-file* (count-lines-in-file (drawing-options-file-path *drawing-options*)))
	    (*ps-doc*        (and *lines-in-file*
				  (make-instance 'ps:psdoc
						 :page-size (strip-size *lines-in-file*)
						 :writeproc (cffi:callback write-to-stdout)))))
       (if *ps-doc*
	   (progn
	     (setup-ps-doc)
	     (with-open-file (stream (drawing-options-file-path *drawing-options*))
	       (yacc:parse-with-lexer (tokenizer stream) *parser*))
	     (cleanup-ps-doc))
	   (format *error-output*
		   "File ~a is empty~%"
		   (drawing-options-file-path *drawing-options*)))))))
