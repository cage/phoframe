;; mkphoframe: make fortune strips
;; Copyright (C) 2016  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :phoframe)

(defun count-lines-in-file (file)
  (with-open-file (stream file :direction :input :if-does-not-exist :error)
    (when stream
      (do ((line (read-line stream nil nil) (read-line stream nil nil))
	   (ct 0 (if (not (scan +comment-re+ line))
		     (1+ ct)
		     ct)))
	  ((not line)  (if (> ct 0)
			   ct
			   nil))))))

(defun unescape-string (s)
  (regex-replace-all +escaped-newline+
		     s
		     (format nil "~%")))

(cl-lex:define-string-lexer lexer
  ("^#.*"                  (return (values 'comment $@)))
  ("^(\\./|/).+\\.jpg"     (return (values 'path $@)))
  ("^[\\\"A-Za-z0-9\\[].+" (return (values 'fortune (unescape-string $@)))))

(defun tokenizer (stream)
  (let ((line-count 0))
  #'(lambda ()
      (let ((line (read-line stream nil nil)))
	(if line
	    (let ((token (funcall (lexer line))))
	      (incf line-count)
	      (if token
		  (funcall (lexer line))
		  (let ((warn-text (format nil
					   "Line ~a starts with an invalid character(s), ignoring"
					   (1- line-count))))
		    (warn warn-text)
		    (values 'comment warn-text))))
	    (values nil nil))))))

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defmacro with-incf-count-boxes (&body body)
    `(unwind-protect
	  (progn
	    ,@body)
       (incf *count-boxes*)))

  (defun yy-draw-fortune-message (a &optional b)
    (declare (ignorable b))
    (with-incf-count-boxes
	(draw-fortune *ps-doc* *count-boxes* a)))

  (defun yy-draw-fortune-image (a &optional b)
    (declare (ignorable b))
    (when (uiop:file-exists-p a)
      (with-incf-count-boxes
	  (draw-image *ps-doc* *count-boxes* a)))))

(yacc:define-parser *parser*
  (:start-symbol db)
  (:terminals (path fortune comment))
  (db
   (path    db #'yy-draw-fortune-image)
   (fortune db #'yy-draw-fortune-message)
   (comment db #'(lambda (a b)
		   (declare (ignore b))
		   (format *error-output* "ignoring comment ~s~%" a)))
   (comment    #'(lambda (a)
		   (format *error-output* "ignoring comment ~s~%" a)))
   (path       #'yy-draw-fortune-message)
   (fortune    #'yy-draw-fortune-message)))
