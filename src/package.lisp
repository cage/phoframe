;; mkphoframe: make fortune strips
;; Copyright (C) 2016  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :config
  (:use :cl)
  (:export))

(defpackage :constants
  (:use :cl
	:alexandria)
  (:export
    :+default-font-name+
   :+default-box-width+
   :+default-box-height+
   :+default-line-width+
   :+top-margin-box+
   :+ends-offset-scale+
   :+side-margin-box+
   :+escaped-newline+
   :+comment-re+
   :+default-starting-message+
   :+default-ending-message+
   :+default-maximum-font-size+
   :+scaling-height-marker-at-end+))

(defpackage :phoframe
  (:use :cl
	:alexandria
	:cl-ppcre
	:constants
	:config)
  (:export
   :main))
