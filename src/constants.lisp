;; mkphoframe: make fortune strips
;; Copyright (C) 2016  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :constants)

(define-constant +default-font-name+     "font"                               :test #'string=)

(define-constant +default-box-width+      170.0                               :test #'=
		 :documentation           "In millimiters")

(define-constant +default-box-height+     100.0                               :test #'=
		 :documentation           "In millimiters")

(define-constant +default-line-width+       2.0                               :test #'=
		 :documentation           "In millimiters")

(define-constant +top-margin-box+          20.0                               :test #'=
		 :documentation           "In millimiters")

(define-constant +side-margin-box+         20.0                               :test #'=
		 :documentation           "In millimiters")

(define-constant +ends-offset-scale+       3.0                                :test #'=
		 :documentation           "Scaling for  +top-margin-box+ at ends of the strip")

(define-constant +escaped-newline+         "\\\\n"                            :test #'string=)

(define-constant +comment-re+               "^#"                              :test #'string=)

(define-constant +default-starting-message+ "Start"                           :test #'string=)

(define-constant +default-ending-message+   "The End"                         :test #'string=)

(define-constant +default-maximum-font-size+ 8.0                              :test #'=)

(define-constant +scaling-height-marker-at-end+ 6.0                           :test #'=)
