;; mkphoframe: make fortune strips
;; Copyright (C) 2016  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :phoframe)

(defun actual-item-count (item-count)
  (+ item-count 2))

(defun ends-offset ()
  (* +ends-offset-scale+  +top-margin-box+))

(defun total-strip-height (item-count)
  (let ((actual-item-count (actual-item-count item-count)))
    (+ (* +default-box-height+ actual-item-count)
       (* +top-margin-box+     (1+ actual-item-count))
       (* 2 (ends-offset)))))

(defun strip-size (item-count)
  (make-instance 'ps:page-size
		 :width  ps:+a4-paper-width+
		 :height (total-strip-height item-count)))

(defun font-dir ()
  (let ((pathname (uiop:merge-pathnames* (drawing-options-font-dir *drawing-options*))))
    (uiop:native-namestring pathname)))

(defun count-box-at-ends-p (ct)
  (or (= ct 0)
      (= ct (1- (actual-item-count *lines-in-file*)))))

(defun bottom-box-y (ct)
  (+ (* ct +default-box-height+)
     (* ct +top-margin-box+)
     +top-margin-box+
     (ends-offset)))

(defun draw-box (doc x y w h)
  (ps:save doc)
  (ps:setcolor doc ps:+color-type-fillstroke+ cl-colors:+red+)
  (ps:rect doc x y w h)
  (ps:stroke doc)
  (ps:restore doc))

(defun draw-markers (doc y ct)
  (let ((marker-length    (drawing-options-marker-item-line-length *drawing-options*))
	(marker-y         (+ y (/ +default-box-height+ 2)))
	(left-marker-x    (+ +side-margin-box+ +default-box-width+))
	(left-marker-ends (+ (* 2 +side-margin-box+) +default-box-width+)))
    (ps:save doc)
    (if (count-box-at-ends-p ct)
	(ps:setlinewidth doc
			 (* +scaling-height-marker-at-end+
			    (drawing-options-marker-item-line-width *drawing-options*)))
	(ps:setlinewidth doc (drawing-options-marker-item-line-width *drawing-options*)))
    (ps:moveto doc 0             marker-y)
    (ps:lineto doc marker-length marker-y)
    (ps:stroke doc)
    (ps:moveto doc left-marker-x    marker-y)
    (ps:lineto doc left-marker-ends marker-y)
    (ps:stroke doc)
    (ps:restore doc)))

(defun draw-fortune (doc ct message)
  (let* ((y                (bottom-box-y ct))
	 (x                +side-margin-box+)
	 (h                +default-box-height+)
	 (w                +default-box-width+)
	 (font-size        (drawing-options-maximum-font-size *drawing-options*)))
    ;; box
    (when (drawing-options-draw-box-p *drawing-options*)
      (draw-box doc x y w h))
    ;; text
    (ps:draw-text-confined-in-box doc
				  (drawing-options-font-name *drawing-options*)
				  message
				  x y w h
				  :maximum-font-size font-size
				  :vertical-align    :center
				  :horizontal-align  ps:+boxed-text-h-mode-center+)
    ;; item markers
    (draw-markers doc y ct)))

(defun accomodate-image (image-w image-h
			 &key
			   (scaling (/ +default-box-width+ image-w)))
  (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (single-float image-w image-h scaling))
  (let* ((scaled-h (* scaling image-h)))
    (if (<= scaled-h +default-box-height+)
	scaling
	(accomodate-image image-w image-h :scaling (- scaling 1e-6))))) ; tail call!

(defun draw-image (doc ct path)
  (multiple-value-bind (image-id w-image h-image)
      (ps:open-image-file *ps-doc*
			  cl-pslib:+image-file-type-jpeg+
			  path
			  "" 0)
    (let* ((y              (bottom-box-y ct))
	   (x              +side-margin-box+)
	   (h              +default-box-height+)
	   (w              +default-box-width+)
	   (scaling        (accomodate-image w-image h-image))
	   (actual-image-w (* w-image scaling))
	   (actual-image-h (* h-image scaling)))
      ;; box
      (when (drawing-options-draw-box-p *drawing-options*)
	(draw-box doc x y w h))
      (ps:place-image doc
		      image-id
		      (- (+ x (/ w 2))
			 (/ actual-image-w 2))

		      (- (+ y (/ h 2))
			 (/ actual-image-h 2))
		      scaling)
      (draw-markers doc y ct))))
